#ifndef CELL_H
#define CELL_H

#include <QtGui/QPushButton>
#include <QString>
#include <QMouseEvent>

#include <iostream>

#define PATH_IMG_FLAG "../flag.png"

enum State:int { unknow, flagged, discovered};

class Cell : public QPushButton {

public:
    Cell(std::string str) : QPushButton(QString::fromStdString(str)) {
        state = unknow;
    };

    virtual bool isMine() = 0;

    virtual void onLClick() = 0;

    virtual void onRClick() = 0;

    virtual void setValue(int){std::cout<<"CELL"<<'\n';};

    void flag(bool);


signals:
    virtual void rightClicked() = 0;

    State state;
protected:
    void mouseReleaseEvent(QMouseEvent *e) {
        if (e->button() == Qt::RightButton)
            emit rightClicked();
        else if (e->button() == Qt::LeftButton)
            emit clicked();
    }
};
#endif // CELL_H