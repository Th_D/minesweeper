#ifndef GUI_H
#define GUI_H

#include <QMainWindow>
#include "cell.h"

namespace Ui {
class Gui;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void addCell(int, int, Cell* cell);



    /*void setScore(int score);

    void setGrid(___);

    void explode();

    void flag();
*/


private:
    Ui::Gui *ui;
    QIcon* iconFlag;

    static const int SIZE_CELL = 20;
};

#endif // GUI_H
