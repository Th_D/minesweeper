#ifndef GAME_H
#define GAME_H

#include "warground.h"
#include "gui.h"

class Game
{
public:
    Game(MainWindow*);

    static const int H_GROUND = 5;
    static const int L_GROUND = 10;

private:
    WarGround warground;
};

#endif // GAME_H
