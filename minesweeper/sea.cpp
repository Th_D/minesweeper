#include <ostream>
#include "sea.h"

Sea::Sea() : Cell("")
{
    connect(this, SIGNAL(clicked()), this, SLOT(onLClick()));
    connect(this, SIGNAL(rightClicked()), this, SLOT(onRClick()));
    this->value = 0;
}

std::ostream& operator<<(std::ostream &strm, const Sea &a) {
    return strm << "_";
}

void Sea::onLClick() {
    std::cout<<"Sea::onLClick"<<'\n';
    if(this->state != discovered){
        this->state = discovered;
        this->setText(QString::number(this->value));
    }
}

void Sea::onRClick() {
    if(this->state != discovered)
        this->flag(this->state == unknow);
    std::cout<<"Sea::onRClick"<<'\n';
}

bool Sea::isMine(){
    return false;
}