#include <ostream>

#include "mine.h"

Mine::Mine() : Cell("X")
{
    connect(this, SIGNAL(clicked()), this, SLOT(onLClick()));
    connect(this, SIGNAL(rightClicked()), this, SLOT(onRClick()));
}


std::ostream& operator<<(std::ostream &strm, const Mine &a) {
    return strm << "X";
}



void Mine::onLClick() {
    std::cout<<"LOOSE !"<<'\n';
    this->state = discovered;
}

void Mine::onRClick() {
    if(this->state != discovered)
        this->flag(this->state == unknow);
    std::cout<<"Mine::onRClick"<<'\n';
}


bool Mine::isMine(){
    return true;
}