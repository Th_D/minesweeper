#include <iostream>

#include "warground.h"
#include "sea.h"
#include "mine.h"
#include "gui.h"

WarGround::WarGround(int h, int l, MainWindow* window)
{
    std::cout<<"Construct WARGROUND ["<<h<<", "<<l<<"]"<<std::endl;

    this->sizeH = h;
    this->sizeL = l;
    int cptMine = 0;
    this->nbMine = (h*l)/4;
    srand (time(NULL));
    std::cout<<"nbMines = "<<this->nbMine<<"\n";
    for(int i = 0; i<h; i++){
        this->grid.emplace_back();
        for(int j = 0; j<l; j++){
            int random  = rand() % (h * l);
            if(random > nbMine) {
                this->grid[i].push_back(new Sea());
                std::cout << "O\t";
            }
            else {
                this->grid[i].push_back(new Mine());
                cptMine++;
                std::cout<<"X\t";
            }
            window->addCell(i, j, this->grid[i][j]);

        }
        std::cout<<'\n';
    }
    this->nbMine = cptMine;
    std::cout<<"nbMines = "<<this->nbMine<<"\n";

    for(int i = 0; i<h; i++) {
        for (int j = 0; j < l; j++) {
            std::cout<< "getNB" <<i<<" "<<j<<" "<< getNb(i,j) <<'\n';
            this->grid[i][j]->setValue(getNb(i,j));
        }
    }
}

int WarGround::getNb(int x, int y) {
    int ret = 0;
    for (int i = x-1; i <= x+1; i++) {
        for (int j = y-1; j <= y+1; j++) {
            if (i >= 0 && j >= 0 && i<this->sizeH && j<this->sizeL && this->grid[i][j]->isMine()) {
                ret++;
            }
            std::cout<<"|";
        }
        std::cout<<'\n';
    }
    return ret;
}
