#ifndef SEA_H
#define SEA_H

#include "cell.h"

class Sea : public Cell
{
    Q_OBJECT
public:
    Sea();
    void setValue(int value) { this->value = value; std::cout<<"Sea::setValue()"<<'\n';}
    virtual bool isMine();


private:
    int value;

signals:
    virtual void rightClicked();

public slots:
    void onLClick();
    void onRClick();
};

#endif // SEA_H
