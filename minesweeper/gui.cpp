#include <iostream>

#include <QtGui/QGridLayout>
#include <QtGui/QPushButton>

#include "gui.h"
#include "ui_gui.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Gui)
{
    ui->setupUi(this);
    //pixmapFlag = new QIcon(new QPixmap("img/flag.png"));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::addCell(int x, int y, Cell *cell) {
    QGridLayout* grid;
    if((grid = qFindChild<QGridLayout*>(this, QString("gridWarGround")))){
        cell->setFixedSize(SIZE_CELL, SIZE_CELL);
        grid->addWidget(cell, x, y);
    } else {
        std::cout<<"grid layout not found :("<<std::endl;
    }
}


