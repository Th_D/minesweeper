#include <QApplication>

#include <iostream>

#include "gui.h"
#include "game.h"
#include "sea.h"

int main(int argc, char *argv[])
{
c    QApplication a(argc, argv);
    MainWindow* window = new MainWindow();
    Game* game = new Game(window);
    window->show();
    return a.exec();
}
