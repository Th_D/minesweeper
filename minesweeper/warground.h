#ifndef WARGROUND_H
#define WARGROUND_H

#include <vector>
#include <memory>

#include "cell.h"
#include "gui.h"


class WarGround
{
public:
    WarGround(int, int, MainWindow*);

private:
    int sizeH;
    int sizeL;
    int nbMine;
    std::vector<std::vector<Cell*>> grid;

    int getNb(int, int);

};

#endif // WARGROUND_H
