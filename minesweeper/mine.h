#ifndef MINE_H
#define MINE_H

#include "cell.h"

class Mine : public Cell
{
    Q_OBJECT
public:
    Mine();
    virtual bool isMine();

signals:
    virtual void rightClicked();

public slots:
    void onLClick();
    void onRClick();
};

#endif // MINE_H
