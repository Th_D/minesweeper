#include "cell.h"

#include <QIcon>
#include <QtCore/QDir>
#include <QtCore/QDirIterator>

void Cell::flag(bool status) {
    if (status){
        QDirIterator it("../../minesweeper/img/", QStringList(), QDir::Files, QDirIterator::Subdirectories);
        while (it.hasNext())
            std::cout << it.next().toStdString()<<'\n';
        //std::cout<<"Cell::flag(ON) ->setIcon"<<QDir::currentPath().toStdString()<<'\n';
        this->state = flagged;
        this->setIcon(QIcon("../../minesweeper/img/flag.png"));
    }
    else {
        std::cout<<"Cell::flag(OFF)"<<'\n';
        this->state = unknow;
        this->setIcon(QIcon());
    }
}
